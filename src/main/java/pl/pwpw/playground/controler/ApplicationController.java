package pl.pwpw.playground.controler;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.pwpw.playground.controler.dto.ApplicationContactResponse;
import pl.pwpw.playground.controler.dto.ApplicationInfoResponse;
import pl.pwpw.playground.domain.ApplicationService;

import java.util.Optional;

@RestController
@AllArgsConstructor
public class ApplicationController {

    private final ApplicationService applicationService;

    @GetMapping("api/applications/contact")
    public ResponseEntity<ApplicationContactResponse> getApplicationContact(@RequestParam("applicationNumber") String applicationNumber) {
        Optional<ApplicationContactResponse> application = applicationService.getApplicationContact(applicationNumber);
        return application
                .map(applicationContactResponse -> new ResponseEntity<>(applicationContactResponse, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("api/applications")
    public ResponseEntity<ApplicationInfoResponse> getApplicationByEmail(@RequestParam("email") String email) {
        Optional<ApplicationInfoResponse> application = applicationService.getApplicationByEmail(email);
        return application
                .map(applicationContactResponse -> new ResponseEntity<>(applicationContactResponse, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
