package pl.pwpw.playground.controler;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.domain.AttachmentService;

@RestController
@AllArgsConstructor
public class AttachmentController {

    private final AttachmentService attachmentService;

    @PostMapping(value = "api/attachments")
    public ResponseEntity<String> uploadAttachment(
            @RequestParam("file") MultipartFile file,
            @RequestParam("applicationNumber") String applicationNumber) {
        try {
            Long id = attachmentService.uploadAttachment(file, applicationNumber);
            return ResponseEntity.ok().body(id.toString());
        } catch (NotUploadFileException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
