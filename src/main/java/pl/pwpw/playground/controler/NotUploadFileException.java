package pl.pwpw.playground.controler;

public class NotUploadFileException extends Exception {

    public NotUploadFileException(String errorMessage) {
        super(errorMessage);
    }
}
