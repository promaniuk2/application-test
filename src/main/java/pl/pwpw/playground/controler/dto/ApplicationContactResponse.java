package pl.pwpw.playground.controler.dto;

import lombok.Value;

@Value
public class ApplicationContactResponse {
    String emailAddress;
    String phoneNumber;
}
