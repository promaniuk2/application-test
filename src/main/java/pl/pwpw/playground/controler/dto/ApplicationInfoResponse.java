package pl.pwpw.playground.controler.dto;

import lombok.Value;

@Value
public class ApplicationInfoResponse {
    String applicationType;
    String applicationNumber;
    String lastName;
}
