package pl.pwpw.playground.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    Optional<Application> findByApplicationNumber(ApplicationNumber applicationNumber);
    Optional<Application> findByContactDetails_EmailAddress(EmailAddress emailAddress);
}
