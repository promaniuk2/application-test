package pl.pwpw.playground.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.pwpw.playground.controler.dto.ApplicationContactResponse;
import pl.pwpw.playground.controler.dto.ApplicationInfoResponse;


import java.util.Optional;

@Service
@AllArgsConstructor
public class ApplicationService {

    private final ApplicationRepository applicationRepository;

    public Optional<ApplicationContactResponse> getApplicationContact(String applicationId) {
        return applicationRepository.findByApplicationNumber(new ApplicationNumber(applicationId))
                .map(application -> new ApplicationContactResponse(
                        application.getContactDetails().getEmailAddress().getEmailAddress(),
                        application.getContactDetails().getPhoneNumber().getPhoneNumber()));
    }

    public Optional<ApplicationInfoResponse> getApplicationByEmail(String emailAddress) {
        return applicationRepository.findByContactDetails_EmailAddress(new EmailAddress(emailAddress))
                .map(application -> new ApplicationInfoResponse(
                        application.getApplicationType().toString(),
                        application.getApplicationNumber().getApplicationNumber(),
                        application.getLastName()));
    }
}
