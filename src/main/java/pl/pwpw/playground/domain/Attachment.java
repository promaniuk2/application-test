package pl.pwpw.playground.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Blob;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@Entity
public class Attachment {

    @Id
    @SequenceGenerator(name = "attachment_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attachment_id_seq")
    private Long id;

    @Column(name="content")
    @Lob
    private Blob content;

    private LocalDateTime uploadTime;

    @Enumerated(EnumType.STRING)
    private AttachmentType attachmentType;

    @ManyToOne
    @JoinColumn(name = "appId")
    private Application application;

}
