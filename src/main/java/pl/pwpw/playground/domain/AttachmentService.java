package pl.pwpw.playground.domain;

import lombok.AllArgsConstructor;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pl.pwpw.playground.controler.NotUploadFileException;

import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;
    private final ApplicationRepository applicationRepository;

    public Long uploadAttachment(MultipartFile file, String applicationNumber) throws NotUploadFileException {

        AttachmentType attachmentType;
        try {
            attachmentType = AttachmentType.valueOf(FilenameUtils.getExtension(file.getOriginalFilename()).toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new NotUploadFileException("Incorrect file extension");
        }

        Blob blob;
        try {
            blob = new SerialBlob(file.getBytes());
        } catch (IOException | SQLException throwables) {
            throw new NotUploadFileException("the file was not loaded");
        }

        Application application = applicationRepository.findByApplicationNumber(new ApplicationNumber(applicationNumber))
                .orElseThrow(() -> new NotUploadFileException("Not found application for given applicationNumber"));

        Attachment attachment = new Attachment();
        attachment.setAttachmentType(attachmentType);
        attachment.setApplication(application);
        attachment.setUploadTime(LocalDateTime.now());
        attachment.setContent(blob);
        return attachmentRepository.save(attachment).getId();
    }
}
