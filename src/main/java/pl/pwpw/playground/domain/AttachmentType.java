package pl.pwpw.playground.domain;

enum AttachmentType {
    PNG,
    PDF
}
